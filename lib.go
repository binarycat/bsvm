package bsvm



var emptyLib *Func = nil


func (f *Func) F(name string, body []byte) *Func {
	return &Func{
		Name: []byte(name),
		Code: body,
		Dict: f,
	}
}

func (f *Func) I(lib *Func) *Func {
	if lib == nil { return f }
	r := *lib
	r.Dict = f.I(lib.Dict)
	return &r
}

func comp(args ...any) []byte {
	var r []byte
	for _, arg := range args {
		switch v := arg.(type) {
		case int:
			r = append(r, byte(v))
		case byte:
			r = append(r, v)
		case []byte:
			r = append(r, v...)
		case string:
			// BUG: comp() cannot handle strings larger than 256 bytes
			r = append(r, I_LITL1, byte(len(v)))
			r = append(r, v...)
		default:
			panic("unexpeted type passed to comp()")
		}
	}
	return r
}

var u64Lib = emptyLib.
	F("u64+", comp(I_ADD, I_LIT1, 8, I_PRE))

var u32Lib = emptyLib.
	// TODO: most of this isn't actually 32 bit, put it in a different lib
	F("u32+", comp(I_ADD, I_LIT1, 4, I_PRE)).
	F("u32-", comp(
		I_LIT4, 0xFF, 0xFF, 0xFF, 0xFF, I_XOR,
		I_LIT1, 1, I_ADD,
		I_ADD, I_LIT1, 4, I_PRE)).
	F("_u32*", comp( // ( a dst b -- a dst b )
		I_TRIM, I_CON, I_LIT1, 1, I_SUB,
		I_SWAP, I_LIT1, 2, I_PICK,
		I_ADD, I_SWAP, I_DUP, I_WHILE)).
	F("u32*", comp( // ( a b -- a*b )
		I_LIT1, 0, I_SWAP, "_u32*", I_CALL,
		I_DROP, I_NIP)).
	F("_u32%", comp( // ( b a -- a%b x x x )
		I_OVER, I_OVER, I_SWAP, I_SUB, I_CARRY, // b a a-b carry
		I_LITL1, 0, I_EQL, I_CON,
		I_DROP, I_NIP, I_DUP, I_WHILE, I_ERR)).
	F("u32%", comp( // ( a b -- a%b )
		I_SWAP, "_u32%", I_CALL, I_DROP, I_DROP, I_NIP))

var mathLib = emptyLib.
	F("_mul", comp( // ( a dst b -- a dst b )
		I_TRIM, I_CON, I_LIT1, 1, I_SUB,
		I_SWAP, I_LIT1, 2, I_PICK,
		I_ADD, I_SWAP, I_DUP, I_WHILE)).
	F("mul", comp( // ( a b -- a*b )
		I_LIT1, 0, I_SWAP, "_mul", I_CALL,
		I_DROP, I_NIP)).
	F("_mod", comp( // ( b a -- a%b x x x )
		I_OVER, I_OVER, I_SWAP, I_SUB, I_CARRY, // b a a-b carry
		I_LITL1, 0, I_EQL, I_CON,
		I_DROP, I_NIP, I_DUP, I_WHILE, I_ERR)).
	F("mod", comp( // ( a b -- a%b )
		I_SWAP, "_mod", I_CALL, I_DROP, I_DROP, I_NIP))

var boolLib = emptyLib.
	F("false", []byte{I_LITL1, 0}).
	// everything not false is truthy, but this is the canon "true" value.
	F("true", []byte{I_LIT1, 1}).
	// boolean operations
	F("b!", []byte{I_LITL1, 0, I_LIT1, 1, I_IF}).
	F("b&", []byte{I_LITL1, 0, I_IF}).
	F("b|", []byte{I_LIT1, 1, I_SWAP, I_IF})

var utilLib = emptyLib.
	F("noop", comp()).
	F("last-byte", comp(I_DUP, I_LEN, I_LIT1, 1, I_SUB, I_SUF)).
	F("all-but-last-byte", comp(I_DUP, I_LEN, I_LIT1, 1, I_SUB, I_PRE)).
	F("trim-trailing", comp( // ( byte str -- str )
		I_OVER, I_OVER, "last-byte", I_CALL, // byte str byte last
		I_EQL, "all-but-last-byte", "noop", I_IF, I_CALL,
		I_DUP, I_WHILE, I_NIP)).
	// OBSOLETE!!
	F("trim-trailing-zeros", comp(I_LIT1, 0, I_SWAP, "trim-trailing", I_CALL)).
	F("u=", comp(
		"trim-trailing-zeros", I_CALL, I_SWAP,
		"trim-trailing-zeros", I_CALL, I_EQL)).
	F("_reverse", comp( // dst src -- dst src
		// remove byte from the start of src
		I_LIT1, 1, I_CUT, // dst byte src'
		// add byte to start of dst
		I_LIT1, 2, I_FLIP, // src' byte dst
		I_CAT, I_SWAP, // dst' src'
		I_DUP, I_WHILE)).
	F("reverse", comp( // str -- rts
		"", I_SWAP, "_reverse", I_CALL, I_DROP))


var md5Lib = emptyLib.
	I(mathLib).
	F("rol", comp(I_SHL, I_CARRY, I_OR)).
	F("||", comp( // a b -- a||b
		I_OVER, I_SWAP, I_IF)).
	F("_md5pad", comp( // msg -- msg _
		I_DUP, I_LEN, // msg len
		I_LIT1, 64, "mod", I_CALL, // msg modlen
		I_LIT1, 56, I_SWAP, I_SUB, I_TRIM, I_CON, // msg sublen
		I_SWAP, I_LIT1, 0, I_CAT, // sublen msg'
		I_SWAP, I_WHILE, I_ERR)).
	// append padding bytes and length of message
	F("md5pad", comp( // msg -- paddedmsg
		I_DUP, I_LEN, I_SWAP, I_LIT1, 128, I_CAT, // oldlen msg
		"_md5pad", I_CALL, I_DROP, // oldlen paddedmsg
		I_SWAP, I_LITL1, 8, 0, 0, 0, 0, 0, 0, 0, 0,
		I_ADD,
		I_LIT1, 3, I_SHL, I_CAT)).
	F("md5_s", comp( // idx -- shift
		I_LITL1, 64,
		7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
		5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
		4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
		6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,
		I_SWAP, I_SUF, I_LIT1, 1, I_PRE)).
	F("md5_K", comp( // idx -- K
		I_LITL2, 0, 1,
		0x78, 0xa4, 0x6a, 0xd7, 0x56, 0xb7, 0xc7, 0xe8, 0xdb, 0x70, 0x20, 0x24, 0xee, 0xce, 0xbd, 0xc1,
		0xaf, 0x0f, 0x7c, 0xf5, 0x2a, 0xc6, 0x87, 0x47, 0x13, 0x46, 0x30, 0xa8, 0x01, 0x95, 0x46, 0xfd,
		0xd8, 0x98, 0x80, 0x69, 0xaf, 0xf7, 0x44, 0x8b, 0xb1, 0x5b, 0xff, 0xff, 0xbe, 0xd7, 0x5c, 0x89,
		0x22, 0x11, 0x90, 0x6b, 0x93, 0x71, 0x98, 0xfd, 0x8e, 0x43, 0x79, 0xa6, 0x21, 0x08, 0xb4, 0x49,
		0x62, 0x25, 0x1e, 0xf6, 0x40, 0xb3, 0x40, 0xc0, 0x51, 0x5a, 0x5e, 0x26, 0xaa, 0xc7, 0xb6, 0xe9,
		0x5d, 0x10, 0x2f, 0xd6, 0x53, 0x14, 0x44, 0x02, 0x81, 0xe6, 0xa1, 0xd8, 0xc8, 0xfb, 0xd3, 0xe7,
		0xe6, 0xcd, 0xe1, 0x21, 0xd6, 0x07, 0x37, 0xc3, 0x87, 0x0d, 0xd5, 0xf4, 0xed, 0x14, 0x5a, 0x45,
		0x05, 0xe9, 0xe3, 0xa9, 0xf8, 0xa3, 0xef, 0xfc, 0xd9, 0x02, 0x6f, 0x67, 0x8a, 0x4c, 0x2a, 0x8d,
		0x42, 0x39, 0xfa, 0xff, 0x81, 0xf6, 0x71, 0x87, 0x22, 0x61, 0x9d, 0x6d, 0x0c, 0x38, 0xe5, 0xfd,
		0x44, 0xea, 0xbe, 0xa4, 0xa9, 0xcf, 0xde, 0x4b, 0x60, 0x4b, 0xbb, 0xf6, 0x70, 0xbc, 0xbf, 0xbe,
		0xc6, 0x7e, 0x9b, 0x28, 0xfa, 0x27, 0xa1, 0xea, 0x85, 0x30, 0xef, 0xd4, 0x05, 0x1d, 0x88, 0x04,
		0x39, 0xd0, 0xd4, 0xd9, 0xe5, 0x99, 0xdb, 0xe6, 0xf8, 0x7c, 0xa2, 0x1f, 0x65, 0x56, 0xac, 0xc4,
		0x44, 0x22, 0x29, 0xf4, 0x97, 0xff, 0x2a, 0x43, 0xa7, 0x23, 0x94, 0xab, 0x39, 0xa0, 0x93, 0xfc,
		0xc3, 0x59, 0x5b, 0x65, 0x92, 0xcc, 0x0c, 0x8f, 0x7d, 0xf4, 0xef, 0xff, 0xd1, 0x5d, 0x84, 0x85,
		0x4f, 0x7e, 0xa8, 0x6f, 0xe0, 0xe6, 0x2c, 0xfe, 0x14, 0x43, 0x01, 0xa3, 0xa1, 0x11, 0x08, 0x4e,
		0x82, 0x7e, 0x53, 0xf7, 0x35, 0xf2, 0x3a, 0xbd, 0xbb, 0xd2, 0xd7, 0x2a, 0x91, 0xd3, 0x86, 0xeb,
		I_SWAP,
		I_LIT1, 2, I_SHL,
		I_SUF, I_LIT1, 4, I_PRE)).
	F("md5main1", comp( // B C D i -- B C D i F g
		I_LIT1, 3, I_PICK, I_LIT1, 3, I_PICK, I_AND, // B C D i -- B C D i B&C
		I_LIT1, 4, I_PICK, I_INV, // B C D i B&C ~B
		I_LIT1, 3, I_PICK, I_AND, I_OR, // B C D i F
		I_OVER,
	)).
	F("md5main2", comp( // B C D i -- B C D i F g
		I_LIT1, 3, I_PICK, I_LIT1, 2, I_PICK, I_AND, // B C D i B&D
		I_LIT1, 2, I_PICK, I_INV, // B C D i B&D ~D
		I_LIT1, 4, I_PICK, // B C D i B&D ~D C
		I_AND, I_OR, // B C D i F
		I_OVER, I_LIT1, 5, "mul", I_CALL, I_LIT1, 1, I_ADD,
		I_LIT1, 15, I_AND,
	)).
	F("md5main3", comp( // B C D i -- B C D i F g
		I_LIT1, 3, I_PICK, I_LIT1, 3, I_PICK, I_LIT1, 3, I_PICK,
		I_XOR, I_XOR, // B C D i F
		I_OVER, I_LIT1, 3, "mul", I_CALL, I_LIT1, 5, I_ADD,
		I_LIT1, 15, I_AND,
	)).
	F("md5main4", comp( // B C D i -- B C D i F g
		I_OVER, I_INV, // B C D i ~D
		I_LIT1, 4, I_PICK, I_OR, // B C D i (~D)|B
		I_LIT1, 3, I_PICK, I_XOR, // B C D i F
		I_OVER, I_LIT1, 7, "mul", I_CALL,
		I_LIT1, 15, I_AND,
	)).
	F("md5main", comp( // M A B C D i -- M A B C D i
		// use different function depending on value on i
		I_DUP, I_LIT1, 16, I_SUB, I_CARRY, I_NIP, "md5main1", "", I_IF, // M A B C D i fn
		I_OVER, I_LIT1, 32, I_SUB, I_CARRY, I_NIP, "md5main2", "", I_IF, "||", I_CALL,
		I_OVER, I_LIT1, 48, I_SUB, I_CARRY, I_NIP, "md5main3", "", I_IF, "||", I_CALL,
		"md5main4", "||", I_CALL,

		I_CALL, // M A B C D i F g
		// calc new val for F
		I_LIT1, 2, I_SHL, I_LIT1, 7, I_PICK, // M A B C D i F g*4 M
		I_SWAP, I_SUF, I_LIT1, 4, I_PRE, // M A B C D i F M[g]
		I_ADD, I_OVER, // M A B C D i F+M[g] i
		"md5_K", I_CALL, I_ADD, // M A B C D i F+M[g]+K[i]
		I_LIT1, 5, I_PICK, I_ADD, // M A B C D i F+M[g]+K[i]+A
		// new values for A B C D
		I_LIT1, 2, I_PICK, // M A B C D i F' A'
		I_LIT1, 2, I_PICK, "md5_s", I_CALL, // M A B C D i F' A' s[i]
		I_LIT1, 2, I_PICK, // M A B C D i F' A' s[i] F'
		I_SWAP, "rol", I_CALL, // M A B C D i F' A' leftrotate(F',s[i])
		I_LIT1, 6, I_PICK, I_ADD, // M A B C D i F' A' B'		
		I_LIT1, 6, I_PICK, // M A B C D i F' A' B' C'
		I_LIT1, 6, I_PICK, // M A B C D i F' A' B' C' D'
		// flip new values into place
		I_LIT1, 6, I_FLIP, I_DROP,
		I_LIT1, 6, I_FLIP, I_DROP,
		I_LIT1, 6, I_FLIP, I_DROP,
		I_LIT1, 6, I_FLIP, I_DROP,
		// discard F'
		I_DROP,
		// loop
		I_LIT1, 1, I_ADD,
		I_LIT1, 64, I_OVER, I_SUB, I_TRIM, I_WHILE,
	)).
	F("md5loop", comp( // a0 b0 c0 d0 msg -- a0 b0 c0 d0 msg
		I_DUP, I_LIT1, 64, I_SUF, I_SWAP, I_LIT1, 64, I_PRE, // a0 b0 c0 d0 rmsg M
		I_LIT1, 5, I_PICK, I_LIT1, 5, I_PICK, I_LIT1, 5, I_PICK, I_LIT1, 5, I_PICK, // a0 b0 c0 d0 rmsg M A B C D
		I_LIT1, 0,
		"md5main", I_CALL, // a0 b0 c0 d0 rmsg M A B C D i
		I_DROP, // a0 b0 c0 d0 rmsg M A B C D
		I_LIT1, 6, I_PICK, I_ADD, // a0 b0 c0 d0 rmsg M A B C d0+D
		I_LIT1, 6, I_FLIP, I_DROP, // a0 b0 c0 d0+D rmsg M A B C
		I_LIT1, 6, I_PICK, I_ADD, I_LIT1, 6, I_FLIP, I_DROP, // a0 b0 c0+C d0+D rmsg M A B
		I_LIT1, 6, I_PICK, I_ADD, I_LIT1, 6, I_FLIP, I_DROP, // a0 b0+B c0+C d0+D rmsg M A
		I_LIT1, 6, I_PICK, I_ADD, I_LIT1, 6, I_FLIP, I_DROP, // a0+A b0+B c0+C d0+D rmsg M
		I_DROP, I_DUP, I_WHILE)).
	// THIS DOES NOT CURRENTLY MATCH THE OUTPUT OF THE ACTUAL MD5 HASH FUNCTION
	F("md5sum", comp( // msg -- hash
		"md5pad", I_CALL,
		I_LIT4, 0x01, 0x23, 0x45, 0x67, I_SWAP,
		I_LIT4, 0x89, 0xab, 0xcd, 0xef, I_SWAP,
		I_LIT4, 0xfe, 0xdc, 0xba, 0x98, I_SWAP,
		I_LIT4, 0x76, 0x54, 0x32, 0x10, I_SWAP,
		// a0 b0 c0 d0 msg
		"md5loop", I_CALL, // a0 b0 c0 d0 msg
		I_DROP, I_CAT, I_CAT, I_CAT))

// table of bsvm programs for calculating various hash functions
var Hash = map[string][]byte{
	"md5": packLibCall(md5Lib, "md5sum"),
}




// TODO: run-length encoding and other compression methods

