// ByteString Virtual Machine

package bsvm

type Func struct {
	Name []byte
	Code []byte
	// previously defined funcs
	Dict *Func
}

type Frame struct {
	*Func
	Pos int
	// smallest value passed to I_WHILE
	// in order to loop again, it must get smaller
	While []byte
}


type VM struct {
	Dict *Func
	Code []byte
	PS [][]byte
	RS []Frame
	Carry []byte
}
