package bsvm

// TODO: maybe reorder these into something a bit more neat?
const (
	// short lits, have a fixed length
	I_LIT1 byte = iota
	I_LIT2
	I_LIT4
	I_LIT8
	// long lits, have a fixed size length specifyer, then a number of bytes
	I_LITL1
	I_LITL2
	I_LITL4
	I_LITL8

	// dict operations
	I_FUNC // define a function
	I_CALL // call the function once
	I_BODY // retrive the body of a function

	// string operations
	I_CAT
	I_CUT
	I_LEN
	I_EQL
	I_TRIM

	// stack operations
	I_DUP
	I_DROP
	I_SWAP
	I_OVER
	I_NIP
	I_PICK
	// generic version of swap, like how pick is the generic version of dup
	I_FLIP

	// arithmetic operations
	I_ADD
	I_SUB
	// bit shift left (multiply by 2^n).  returns result and carry.
	I_SHL

	// bitwise operations
	I_AND
	I_OR
	I_XOR
	I_INV // bitwise not

	// control flow
	I_CON // conditional return, continue if nonnil
	I_WHILE
	I_IF
	I_CARRY // get contents of carry register
	I_ERR // abort the program with an error
	I_DBG // does not affect the vm itself, but may cause the host to trigger an implementation-defined debug hook
	I_NOOP
	
	// start of unused opcode space
	I_CUSTOM	
)

// legacy aliases
var (
	I_PRE = []byte{I_CUT, I_DROP}
	I_SUF = []byte{I_CUT, I_NIP}
)

// there is no way to jump into the middle of a literal, so the bytecode can be confidently parsed without ambiguity.

