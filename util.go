package bsvm

import "bytes"

// compares two bytestrings as integers.
// if a is less than b, returns a negative integer
// if a is greater than b, returns a positve integer
// if a and b are equal, returns 0
func compare(a, b []byte) int {
	if len(a) != len(b) { panic("broken assumption") }
	l := len(a)
	for i := range a {
		// bytes are least-significat first, so we iterate backwards
		aa, bb := a[l-1-i], b[l-1-i]
		d := int(aa) - int(bb)
		if d != 0 { return d }
	}
	return 0
}

func packLit(v []byte) []byte {
	if len(v) == 1 {
		return append([]byte{I_LIT1}, v...)
	} else if len(v) == 4 {
		return append([]byte{I_LIT4}, v...)
	} else if len(v) < 256 {
		return append([]byte{I_LITL1, byte(len(v))}, v...)
	} else if len(v) < 0x10000 {
		r := []byte{I_LITL2}
		r = append(r, fromint(len(v))...)
		r = append(r, v...)
		return r
	} else {
		// TODO: ability to pack literals of any size
		panic("literal too big to pack")
	}
}

func packLib(lib *Func) []byte {
	r := []byte{}
	for f := lib; f != nil; f = f.Dict {
		r = bytes.Join(
			[][]byte{
				packLit(f.Code),
				packLit(f.Name),
				{I_FUNC},
				r,
			}, []byte{})
	}
	return r	
}

func packLibCall(lib *Func, fn string) []byte {
	r := packLib(lib)
	r = append(r, packLit([]byte(fn))...)
	r = append(r, I_CALL)
	return r
}
