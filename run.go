package bsvm

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

func touint64(v []byte) uint64 {
	var n uint64
	// TODO: this should panic on overflow i think..
	for i, b := range v {
		n |= uint64(b) << (i*8)
	}
	return n
}

func toint(v []byte) int {
	return int(touint64(v))
}

func fromuint64(n uint64) []byte {
	v := make([]byte, 8)
	binary.LittleEndian.PutUint64(v, n)
	// remove trailing zeros
	for len(v) > 1 && v[len(v)-1] == 0 {
		v = v[0:len(v)-1]
	}
	return v
}

func fromint(n int) []byte {
	return fromuint64(uint64(n))
}

func (fr *Frame) Lit(n int) []byte {
	res := fr.Code[fr.Pos:fr.Pos+n]
	fr.Pos += n
	return res
}

func (vm *VM) Peek(idx int) []byte {
	if 0 >= len(vm.PS) { panic("stack underflow!") }
	return vm.PS[len(vm.PS)-1-idx]
}

func (vm *VM) Poke(idx int, v []byte) {
	vm.PS[len(vm.PS)-1-idx] = v
}

func (vm *VM) Pop() []byte {
	ret := vm.Peek(0)
	vm.PS = vm.PS[0:len(vm.PS)-1]
	return ret
}

func (vm *VM) Push(v []byte) {
	vm.PS = append(vm.PS, v)
}

func (vm *VM) BiOp() (a, b, c []byte) {
	b = vm.Pop()
	a = vm.Pop()
	cl := len(a)
	if len(b) > cl {
		cl = len(b)
	}
	c = make([]byte, cl)
	return a, b, c
}

func (vm *VM) BiOpMap(f func (byte, byte) byte) []byte {
	a, b, c := vm.BiOp()
	for i := range c {
		var aa, bb byte
		if i < len(a) { aa = a[i] }
		if i < len(b) { bb = b[i] }
		c[i] = f(aa, bb)
	}
	return c
}

func (vm *VM) Step() {
	var instr byte
	var errmsg string
	top := &vm.RS[len(vm.RS)-1]
	if top.Pos < len(top.Code) {
		instr = top.Code[top.Pos]
		top.Pos += 1
	} else {
		vm.RS = vm.RS[0:len(vm.RS)-1]
		vm.Carry = []byte{}
		return
	}
	if instr == I_CARRY {
		vm.Push(vm.Carry)
	}
	vm.Carry = []byte{}
	switch(instr) {
	case I_NOOP:
		// do nothing
	case I_IF:
		ifFalse := vm.Pop()
		ifTrue := vm.Pop()
		cond := vm.Pop()
		if len(cond) == 0 {
			vm.Push(ifFalse)
		} else {
			vm.Push(ifTrue)
		}
	case I_WHILE:
		// the main looping construct.
		// all these complicated checks exist to make infinite loops impossible.
		cond := vm.Pop()
		// new length, old length
		nl, ol := len(cond), len(top.While)
		// empty string is always falsy
		if len(cond) == 0 { break }
		// if this is the first WHILE instruction, it is always allowed
		if len(top.While) == 0 { goto W }
		// getting shorter?
		if nl < ol { goto W }
		// getting longer?
		if nl > ol { break }
		// integer value is not getting smaller?
		if compare(cond, top.While) >= 0 { break }
	W:
		top.While = cond
		top.Pos = 0
	case I_CARRY:
		// do nothing, handled elsewhere
	case I_CON:
		if len(vm.Peek(0)) == 0 {
			vm.RS = vm.RS[0:len(vm.RS)-1]
		}
	case I_ERR:
		panic("error instruction reached")
	case I_DBG:
		fmt.Printf("%q pos %d PS<%d>: %v\n", top.Name, top.Pos, len(vm.PS), vm.PS)
	case I_LIT1, I_LIT2, I_LIT4, I_LIT8:
		vm.Push(top.Lit(int(1<<(instr-I_LIT1))))
	case I_LITL1, I_LITL2, I_LITL4, I_LITL8:
		vm.Push(top.Lit(int(toint(top.Lit(int(1<<(instr-I_LITL1)))))))
	case I_FUNC:
		name := vm.Pop()
		body := vm.Pop()
		dict := &Func{
			Dict: vm.Dict,
			Name: name,
			Code: body,
		}
		vm.Dict = dict
	case I_CALL, I_BODY:
		// semantically, this is an early-bound system
		name := vm.Pop()
		dict := top.Dict
		if len(vm.RS) == 1 {
			dict = vm.Dict
		}
		for dict != nil && !bytes.Equal(dict.Name, name) {
			dict = dict.Dict
		}
		if dict == nil {
			errmsg = fmt.Sprintf("undefined function %q!", name)
			goto Err
		}
		if instr == I_BODY {
			vm.Push(dict.Code)
			break;
		}
		vm.RS = append(vm.RS, Frame{ Func: dict, Pos: 0 })
	case I_CAT:
		end := vm.Pop()
		bgn := vm.Pop()
		dst := make([]byte, 0, len(bgn)+len(end))
		dst = append(dst, bgn...)
		dst = append(dst, end...)
		vm.Push(dst)
	case I_CUT:
		l := toint(vm.Pop())
		s := vm.Pop()
		if l > len(s) { l = len(s) }
		vm.Push(s[0:l])
		vm.Push(s[l:len(s)])
	case I_LEN:
		vm.Push(fromint(len(vm.Pop())))
	case I_EQL:
		a := vm.Pop()
		b := vm.Pop()
		if bytes.Equal(a, b) {
			vm.Push([]byte{1})
		} else {
			vm.Push([]byte{})
		}
	case I_TRIM:
		v := vm.Pop()
		for len(v) > 0 && v[len(v)-1] == 0 {
			v = v[:len(v)-1]
		}
		vm.Push(v)
	case I_DUP:
		vm.Push(vm.Peek(0))
	case I_DROP:
		_ = vm.Pop()
	case I_SWAP, I_NIP:
		a := vm.Pop()
		b := vm.Pop()
		vm.Push(a)
		if instr == I_SWAP {
			vm.Push(b)
		}
	case I_OVER:
		vm.Push(vm.Peek(1))
	case I_PICK:
		vm.Push(vm.Peek(toint(vm.Pop())))
	case I_FLIP:
		idx := toint(vm.Pop())
		a := vm.Peek(0)
		b := vm.Peek(idx)
		vm.Poke(0, b)
		vm.Poke(idx, a)
	case I_ADD, I_SUB:
		a, b, c := vm.BiOp()
		carry := int16(0)
		for i := 0; i < len(c); i += 1 {
			var aa, bb byte
			var digit int16
			if i < len(a) { aa = a[i] }
			if i < len(b) { bb = b[i] }
			if instr == I_ADD {
				digit = int16(aa) + int16(bb) + carry
			} else {
				digit = int16(aa) - int16(bb) + carry
			}
			c[i] = byte(digit&0xff)
			carry = digit >> 8
		}
		// TODO: currently addition returns a nil carry sometimes, whereas SHL will instead usually return a zero carry.  should this be made more consistant?
		if carry != 0 {
			vm.Carry = []byte{ byte(carry) }
		}
		vm.Push(c)
	case I_SHL:
		n := toint(vm.Pop()) // exponent
		x := vm.Pop() // number to shift
		c := (n-1)/8+1 // number of carry bytes
		r := make([]byte, len(x)+c) // result+carry
		for i := 0; i < 8*len(x); i += 1 {
			if x[i>>3] & (1<<(i&7)) != 0 {
				r[(i+n)>>3] |= 1<<((i+n)&7)
			}
		}
		vm.Push(r[:len(x)]) // result
		vm.Carry = r[len(x):]
		//fmt.Printf("%v\n", r)
	case I_INV:
		v := vm.Pop()
		r := make([]byte, len(v))
		for i := range v {
			r[i] = v[i]^0xFF
		}
		vm.Push(r)
	case I_XOR:
		vm.Push(vm.BiOpMap(func(a, b byte) byte { return a ^ b }))
	case I_AND:
		vm.Push(vm.BiOpMap(func(a, b byte) byte { return a & b }))
	case I_OR:
		vm.Push(vm.BiOpMap(func(a, b byte) byte { return a | b }))
	default:
		// TODO: it would be cool to let new opcodes be defined by adding 1-byte entries to the dict.  using opcodes that are not part of the core would then instead cause a dict lookup, and only after that failed would the program crash.
		errmsg = fmt.Sprintf("unknown instruction %d at pos %d in %q %#v", instr, top.Pos, top.Name, top.Code)
		goto Err
	}
	return
Err:
	panic(fmt.Sprintf("%s\n\nworking stack: %#v", errmsg, vm.PS))
}

func (vm *VM) Run() {
	for len(vm.RS) > 0 {
		vm.Step()
	}
}

func NewVM(code []byte) *VM {
	return &VM{
		Code: code,
		RS: []Frame{{Func: &Func{ Code: code }}},
	}
}
