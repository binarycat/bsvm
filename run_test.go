package bsvm

import "testing"
import "bytes"

func assertPS(t *testing.T, vm *VM, ps [][]byte) {
	vm.Run()
	if len(ps) != len(vm.PS) {
		t.Errorf("assertPS(): wrong number of results (expected %d, got %d)",
			len(ps),
			len(vm.PS))
	} else {
		for i := range ps {
			if !bytes.Equal(ps[i], vm.PS[i]) {
				t.Errorf(
					"assertPS(): wrong result in cell %d, expected %v, got %v",
					i, ps[i], vm.PS[i])
			}
		}
	}
}

func TestRun(t *testing.T) {
	t.Logf("there are %d opcodes", I_CUSTOM)
	noopVM := NewVM(comp(I_NOOP, I_NOOP, I_NOOP))
	noopVM.Run()
	addVM := NewVM([]byte{I_LIT1, 3, I_LIT1, 5, I_ADD})
	addVM.Run()
	if len(addVM.PS) != 1 || len(addVM.PS[0]) != 1 || addVM.PS[0][0] != 8 {
		t.Error("wrong result from addVM")
	}
	callVM := NewVM([]byte{
		I_LITL1, 3, I_LIT1, 1, I_ADD,
		I_LIT1, '+', I_FUNC,
		I_LIT1, 6,
		I_LIT1, '+', I_CALL,
	})
	callVM.Run()
	if len(callVM.PS) != 1 {
		t.Error("wrong number of results from callVM (expected 1, got ", len(callVM.PS), ")")
	} else if callVM.Pop()[0] != 7 {
		t.Error("wrong result from callVM")
	}
	t.Log("u64VM...")
	u64VM := NewVM(comp(
		I_LIT1, 8,
		I_LIT1, 9,
		"u64+", I_CALL,
	))
	u64VM.Dict = u64Lib
	assertPS(t, u64VM, [][]byte{
		{17},
	})
	t.Log("u32VM...")
	u32VM := NewVM(comp(
		I_LIT1, 5,
		I_LIT1, 3,
		"u32-", I_CALL,
		I_LIT1, 10,
		I_LIT1, 5,
		"u32*", I_CALL,
		I_LIT1, 8,
		I_LIT1, 3,
		"u32%", I_CALL,
		I_LIT1, 9,
		I_LIT1, 3,
		"u32%", I_CALL,
		I_LIT1, 7,
		I_LIT1, 3,
		"u32%", I_CALL,

	))
	u32VM.Dict = u32Lib
	assertPS(t, u32VM, [][]byte{
		{2, 0, 0, 0},
		{50},
		{2},
		{0},
		{1},
	})
	
	t.Log("boolVM...")
	boolVM := NewVM(comp(
		"false", I_CALL,
		"false", I_CALL,
		"b&", I_CALL,
		"false", I_CALL,
		"true", I_CALL,
		"b&", I_CALL,
		"true", I_CALL,
		"false", I_CALL,
		"b&", I_CALL,
		"true", I_CALL,
		"true", I_CALL,
		"b&", I_CALL,
		
		"false", I_CALL,
		"false", I_CALL,
		"b|", I_CALL,
		"false", I_CALL,
		"true", I_CALL,
		"b|", I_CALL,
		"true", I_CALL,
		"false", I_CALL,
		"b|", I_CALL,
		"true", I_CALL,
		"true", I_CALL,
		"b|", I_CALL,
		
		"true", I_CALL,
		"b!", I_CALL,

		"false", I_CALL,
		"b!", I_CALL,
	))
	boolVM.Dict = boolLib
	assertPS(t, boolVM, [][]byte{
		{},
		{},
		{},
		{1},

		{},
		{1},
		{1},
		{1},

		{},
		{1},
	})
	
	t.Log("utilVM...")
	utilVM := NewVM(comp(
		"abcd", "last-byte", I_CALL,

		I_LIT4, 2, 0, 0, 0,
		"trim-trailing-zeros", I_CALL,

		I_LIT4, 7, 0, 1, 0,
		"trim-trailing-zeros", I_CALL,

		I_LIT1, 14,
		I_LIT2, 255, 255,
		I_AND,
	))
	utilVM.Dict = utilLib
	assertPS(t, utilVM, [][]byte{
		{'d'},
		{2},
		{7, 0, 1},
		{14, 0},
	})

	t.Log("testing concatenation...")
	immutVM := NewVM(comp(
		"foo", "bar",
		I_OVER, I_SWAP, I_CAT, // foo foobar
		I_SWAP, "baz", I_CAT, // foobaz foobar
	))
	assertPS(t, immutVM, [][]byte{
		[]byte("foobar"),
		[]byte("foobaz"),
	})

	t.Log("testing bit shifts...")
	shiftVM := NewVM(comp(
		I_LIT1, 3, I_LIT1, 7, I_SHL, I_CARRY,

		I_LIT1, 63, I_LIT1, 2, I_SHL,
	))
	assertPS(t, shiftVM, [][]byte{
		{128},
		{1},
		{252},
	})

	t.Log("testing md5sum...")
	md5VM := NewVM(comp(
		"", "md5sum", I_CALL,
		"test", "md5sum", I_CALL,

		I_LIT1, 17, "md5_s", I_CALL,

		I_LIT1, 0, "md5_K", I_CALL,
		I_LIT1, 1, "md5_K", I_CALL,
		I_LIT1, 63, "md5_K", I_CALL,
		I_LIT1, 62, "md5_K", I_CALL,
	))
	md5VM.Dict = md5Lib
	assertPS(t, md5VM, [][]byte{
		// md5sum of the empty string
		{0xd4, 0x1d, 0x8c, 0xd9, 0x8f, 0x00, 0xb2, 0x04, 0xe9, 0x80, 0x09, 0x98, 0xec, 0xf8, 0x42, 0x7e},
		{0x09, 0x8f, 0x6b, 0xcd, 0x46, 0x21, 0xd3, 0x73, 0xca, 0xde, 0x4e, 0x83, 0x26, 0x27, 0xb4, 0xf6},

		{9},
		{0x78, 0xa4, 0x6a, 0xd7},
		{0x56, 0xb7, 0xc7, 0xe8},
		{0x91, 0xd3, 0x86, 0xeb},
		{0xbb, 0xd2, 0xd7, 0x2a},
	})
	t.Log("testing packLib hashes...")
	hashVM := NewVM(comp(
		packLib(md5Lib),
		"foobar", "md5sum", I_CALL,
	))
	assertPS(t, hashVM, [][]byte{
		{0x38, 0x58, 0xf6, 0x22, 0x30, 0xac, 0x3c, 0x91, 0x5f, 0x30, 0x0c, 0x66, 0x43, 0x12, 0xc6, 0x3f},
	})
}

